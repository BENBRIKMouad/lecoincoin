package lecoincoin

import grails.rest.RestfulController
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_CLIENT',])
class ApiIllustrationController extends RestfulController<Illustration> {
    static responseFormats = ['json', 'xml']


    ApiIllustrationController() {
        super(Illustration,true)
    }

}
