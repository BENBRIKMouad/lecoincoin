package lecoincoin

import grails.plugin.springsecurity.annotation.Secured

class IndexController {
    SaleAdService saleAdService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    @Secured('permitAll')
    def index(Integer max) {
        params.max = Math.min(max ?: 8, 9)
        def salesList = SaleAd.createCriteria().list (params) {
            if ( params.q ) {
                ilike("title", "%${params.q}%")
            }
        }
        if(params.q ){
            respond salesList, model:[q: params.q]
        }else{
            respond saleAdService.list(params), model:[saleAdCount: saleAdService.count()]
        }
    }
    @Secured('permitAll')
    def admin(Integer max) {
        respond saleAdService.list(params), model:[saleAdCount: saleAdService.count()]
    }
}
