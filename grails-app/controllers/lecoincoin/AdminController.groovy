package lecoincoin

import grails.plugin.springsecurity.annotation.Secured

class AdminController {
    SaleAdService saleAdService
    IllustrationService illustrationService
    @Secured(['ROLE_ADMIN'])
    def index() {
        def price =0
        def count=0

        for (def sales in saleAdService.list(params) ){
            if(sales.sold){
                price += sales.price
                count+=1
            }

        }
        def percantage = count/ saleAdService.count() *100

        [
                saleAdCount: saleAdService.count(),
                saleAd: saleAdService.list(params),
                illustrationCount: illustrationService.count(),
                illustration: illustrationService.list(params),
                total_sold:price,
                sold_count:count,
                percantage:percantage


        ]
    }
}
