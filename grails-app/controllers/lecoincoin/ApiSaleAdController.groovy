package lecoincoin

import grails.plugin.springsecurity.annotation.Secured
import grails.rest.Resource
import grails.rest.RestfulController

@Secured(['ROLE_CLIENT',])
class ApiSaleAdController extends RestfulController<SaleAd> {
    static responseFormats = ['json', 'xml']
    ApiSaleAdController() {
        super(SaleAd,true)
    }
}
