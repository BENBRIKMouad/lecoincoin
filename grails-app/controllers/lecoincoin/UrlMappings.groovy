package lecoincoin

import grails.plugin.springsecurity.annotation.Secured

@Secured('permitAll')
class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
       group "/api/v1", {
           "/sales"(controller:"apiSaleAd")
           "/sales/$id"(controller:"apiSaleAd",action: "show")
           "/illustration"(controller:"apiIllustration")
           "/illustration/$id"(controller:"apiIllustration",action: "show")
        }
        "/" {
            controller = "index"
            action = "index"
        }
        "/admin" {
            controller = "index"
            action = "admin"
        }
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
