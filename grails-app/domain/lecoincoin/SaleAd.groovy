package lecoincoin

class SaleAd {
    String title
    String description
    Double price
    Boolean sold = Boolean.FALSE
    Date dateCreated
    Date lastUpdated
    String contact_info
    static hasMany = [ adPicture : Illustration]
    static belongsTo = [owner: User]
    static constraints = {
        title size: 5..40, blank: false,nullable: false
        description size: 5..400,blank: false,nullable: false
        price min: 0D,nullable: false
        contact_info size: 10..255,blank: false,nullable: false
    }
    static mapping = {
        description type: 'text'

    }
    String toString(){
        title
    }
}