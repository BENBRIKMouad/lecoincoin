package lecoincoin

class Illustration {
    String title
    String pictureUrl
    static belongsTo = [saleAd: SaleAd]

    static constraints = {
        title maxSize: 30,nullable: true,blank: true
        pictureUrl nullable: false,blank: false,minSize: 10
    }
    String toString(){
        pictureUrl
    }
}
