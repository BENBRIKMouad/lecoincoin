package lecoincoin

import grails.gorm.transactions.Transactional

@Transactional
class BootStrap {

    def init = { servletContext ->
        def roleAdmin = new Role(authority: 'ROLE_ADMIN').save()
        def userAdmin = new User(username: 'admin', password: 'admin').save()
        UserRole.create userAdmin, roleAdmin

        def roleModerator = new Role(authority: 'ROLE_MODERATOR').save()
        def userModerator = new User(username: 'moderator', password: 'moderator').save()
        UserRole.create userModerator, roleModerator

        def roleClient = new Role(authority: 'ROLE_CLIENT').save()
        def userClient = new User(username: 'client', password: 'client').save()
        UserRole.create userClient, roleClient

        ["Alice","Bob","Charly","David","Etienne"].each {
            String name ->
                def userInstance = new User(username: name, password: "password").save()
                (1..5).each {
                    Integer annonceIdx ->
                        def annonceInstance =
                                new SaleAd(
                                        title: "Annonce $name $annonceIdx",
                                        description: "Description $name $annonceIdx",
                                        price: annonceIdx * 100,
                                        sold: Boolean.FALSE,
                                        contact_info: "contact me at plz $name",
                                        owner: userInstance
                                ).save(failOnError: true)
                        (1..3).each {

                        }
                }
        }


    }
    def destroy = {
    }
}
