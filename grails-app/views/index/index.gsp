<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
</head>
<body>
<content tag="nav">
</content>

<div class="svg" role="presentation">
    <div class="centered" style="background-color: #000000;" >
        <asset:image src="lecoin.png" class="grails-logo" height="200" width="500"/>
    </div>
</div>

<div id="content" role="main">
    <section class="row colset-2-its">
        <h1>Welcome to LeCoin</h1>
    </section>
</div>
<div id="list-saleAd" class="content scaffold-list" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:form action="index" method="post" style="margin: 12px">
    <div class="input-group input-group-lg mb-3">
        <input type="text" class="form-control" placeholder="Search" value="${q}" name="q"/>
        <div class="input-group-append">
            <button class="btn btn-primary"  type="submit">Search</button>
        </div>
    </div>
    </g:form>

    <g:each in="${saleAdList}" var="saleAd" status="idx">
        <div class="card" style="margin-top: 12px">
            <div class="card-header">
                Price ${saleAd.price} $
            </div>
            <div class="card-body" >
                <h5 class="card-title">${saleAd.title}</h5>
                <p class="card-text">${saleAd.description}</p>
                <g:each in="${saleAd.adPicture}" var="all_picture" status="ap_idx">
                    <img src="${all_picture}" alt="not valid or expired url" style="margin: 8px">
                </g:each>
                <br/>
                <a href="saleAd/show/${saleAd.id}" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    </g:each>
    <div class="pagination">
        <g:paginate total="${saleAdCount ?: 0}" />
    </div>
    <div id="controllers" role="navigation">
        <h2>Available Controllers:</h2>
        <ul>
            <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
                <li class="controller">
                    <g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link>
                </li>
            </g:each>
        </ul>
    </div>
</body>
</html>
