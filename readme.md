# lecoincoin #
# By BENBRIK MOUAD ; GADI MOHAMMED AMINE ; SAMI MOHAMMED ZAHID #
## Register Interface ##

![Alt text](pic/register.png?raw=true "Title")

Avialble Roles |
--- |
Admin  
Moderator  
Client

## Login Interface ##
![Alt text](pic/login.png?raw=true "Title")

## Avialable Logins ##

Username  | Password | Role
------------- | ------------- | ------
admin  | admin| ROLE_ADMIN
moderator  | moderator| ROLE_MODERATOR
client | client | ROLE_CLIENT

## index(Home) Interface ##

![Alt text](pic/index.png?raw=true "Title")

## Search Example (annonce with TiTle `bob`) ##

![Alt text](pic/search.png?raw=true "Title")

## Admin Interface (after login with `admin user`) ##
![Alt text](pic/admin2.png?raw=true "Title")
![Alt text](pic/admin.png?raw=true "Title")
indcator  | function
------------- | -------------
ANNONCE COUNT  | count number of annonce that has been sold (sold == TRUE)|
ANNONCES TOTAL  | count the sum of all anonce that has been sold|
PERCANTAGE SOLD | represent the value of annonce that has been sold (ANNONCE SOLD / Total * 100) |

## Sale Ad (Anonce) ##

![Alt text](pic/saladmanage.png?raw=true "Title")

## Api SaleAd ##

![Alt text](pic/api.png?raw=true "Title")

## Api Illustration ##

![Alt text](pic/api2.png?raw=true "Title")

link  | Domaine/function |
----- | ---------|
/saleAd   | saleAd|
/illustration/  | illustration|
/admin/index | admin dashboard |
/ | index |
/api/v1/sales/$id | api saleAd
api/v1/illustration/$id | api illustration
